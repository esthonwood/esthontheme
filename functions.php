<?php

/**
 * Enqueue stylesheet
 * @return [type] [description]
 */
function esthontheme_assets() {
	wp_enqueue_style('style', get_stylesheet_uri());
}
add_action('wp_enqueue_scripts', 'esthontheme_assets');

?>