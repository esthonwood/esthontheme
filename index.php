<?php

$textdomain = 'esthontheme';

get_sidebar();
get_header();
if (have_posts()):
	while (have_posts()): the_post() ?>
		<article class="post">
		<h2><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
		<?php the_content() ?>
		<p>Category: <?php the_category(',') ?> | <?php the_tags() ?> | Posted by: <?php the_author() ?> on <?php the_time() ?></p>
		</article>
<?php
	endwhile;
else:
	echo '<p>'.esc_html__('There are no posts!', $textdomain).'</p>';
endif;

get_footer();
?>